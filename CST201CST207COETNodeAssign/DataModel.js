﻿//Author: 
//Dan Liu, CST207
//Dylan Anderson, CST201

var mongoose = require("mongoose");
var mongoDB = "mongodb://10.20.38.162/DBCST201CST207";
mongoose.connect(mongoDB, { useNewUrlParser: true });
var db = mongoose.connection;
db.on('error', console.error.bind(console, "mongoDB Connection error"));

//Schema for the actor table
var actorSchema = mongoose.Schema({
    firstName: String,
    lastName: String,
    gender: String
});

//Schema for the movie table
var movieSchema = mongoose.Schema({
    name: String,
    leadActor: String,
    leadActress: String,
    avgRating: Number,
    ratingNumber: Number
});

exports.actor = mongoose.model("Actor", actorSchema);
exports.movie = mongoose.model("Movie", movieSchema);