﻿'use strict';
//Author: 
//Dan Liu, CST207
//Dylan Anderson, CST201
//delete all the record from all the table
var model = require('./DataModel');
var obActor = model.actor;
var obMovie = model.movie;

//delete the record from movie table
obMovie.find(function (err, movies) {
    if (err) return console.log(err);
    movies.forEach(x => {
        //crud operation remove
        x.remove(function (err) {
            if (err) console.log(err);
            console.log("movies Removed");
        }
        )
    });
});

//delete all the record from the actor table
obActor.find(function (err, actors) {
    if (err) return console.log(err);
    actors.forEach(x => {
        //crud operation remove
        x.remove(function (err) {
            if (err) console.log(err);
            console.log("actors Removed");
        }
        )
    });
});