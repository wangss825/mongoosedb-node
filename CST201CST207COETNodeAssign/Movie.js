'use strict';
//Author: 
//Dan Liu, CST207
//Dylan Anderson, CST201
var dataModel = require("./DataModel");
var express = require("express");
var path = require("path");

var modActor = dataModel.actor;
var modMovie = dataModel.movie;

var app = express();
var formPath = path.resolve(__dirname, "Forms");
app.use(express.static(formPath));

 //add the actor to the actor database
app.get("/addActor", function (req, res) {
   
    if (req.query.selGender != "0") {
        modActor.create({ firstName: req.query.txtFirst, lastName: req.query.txtLast, gender: req.query.selGender });
        res.writeHead(200, { "Content-Type": "text/html" });
        res.end("Actor " + req.query.txtLast + " has been added to the db");
    }
    //erro condition
    else {
        res.writeHead(200, { "Content-Type": "text/html" });
        res.end("Error: invalid Gender");
    }
});

//From the details page, add the movie with actor,actress, avgRating, ratingNumber to the database
app.get("/details", function (req, res) {

    modMovie.create({ name: req.query.txtMovie, leadActor: req.query.selLeadingActor, leadActress: req.query.selLeadingActress, avgRating: 0, ratingNumber: 0 });
    res.writeHead(200, { "Content-Type": "text/html" });
    res.end("Movie " + req.query.txtMovie + " has been added to the db");
});


//get the data from the actor table  including the firstname, lastname and gender, this will used for the rating page and relation page
app.get("/getJSONActor", (req, res) => {
    modActor.find(function (err, actors) {
        if (err) return console.log(err);

        var retArr = new Array();
        actors.forEach(x => {
            var obVal = { "Name": x.firstName + " " + x.lastName, "Key": x._id, "Gender": x.gender };
            retArr.push(obVal);
        });
        res.writeHead(200, { "content-type": "application/json" });
        res.write(JSON.stringify(retArr));
        res.end();
    });
});

//get the data from the movie table including the leadActor, leadActress and movie name.
app.get("/GetJSONMovie", (req, res) => {
    modMovie.find(function (err, films) {
        if (err) return console.log(err);
        //we want to return an array of JSON objects that describe the authors in the system
        var retArray = new Array();
        films.forEach(x => {
            var obVal = {
                "Name": x.name, "leadActor": x.leadActor, "leadActress": x.leadActress, "Key": x._id
            };
            
            retArray.push(obVal);
        });
        res.json(retArray);
        res.end();

    });
});

//This will return the current average for a movie
app.get('/getCurAvg', (req, res) => {
    console.log(req.query);
    modMovie.findById(req.query.selMovie, function (err, movie) {
        if (err) return console.log(err);

        var ret = movie.avgRating;
        res.writeHead(200, { "content-type": "application/json" });
        res.write(JSON.stringify(ret));
        res.end()
    });
});

//this function will deal with a  user adding a new rating to a movie
app.get('/addRating', (req, res) => {
    if (req.query.txtRating < 0 || req.query.txtRating > 100) return console.log("invalid rating");

    modMovie.findById(req.query.selMovie, function (err, movie) {
        if (err) return console.log(err);

        movie.ratingNumber = movie.ratingNumber += 1; //increase how many time the movie has been rated
        movie.avgRating = ((movie.avgRating * (movie.ratingNumber - 1)) + parseInt(req.query.txtRating)) / (movie.ratingNumber); //update the new average rating
        //console.log(movie.avgRating);
        movie.save();
        //movie.delete();
        //send back new rating
        res.writeHead(200, { 'content-type': 'application/json' });
        res.write("Average Rating is " + JSON.stringify(movie.avgRating));
        res.end();
    });
});

//Catch if an unknown page is requested
app.use((req, res) => {
    res.writeHead(200, { "content-type": "text/html" });
    res.end("Could not find page");
});
app.listen(3009);